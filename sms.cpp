#include "sms.h"

// Init libs
//GSMSSLClient client;
GPRS gprsMMS;
GPRS gprsFREE;
GSM gsmAccess;
GSM_SMS sms;

// Array to hold the
// number a SMS is retreived from
char senderNumber[48 + 1];


/*
   Mobile network setup (GSM & GPRS)

*/
void smsSetup() {
  // GSM connexion
  bool GSMConnected = false;
  while (!GSMConnected) {
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY)) {
      GSMConnected = true;
    } else {
      info("GSM is connecting ...");
      delay(1000);
    }
  }

  // GPRS MMS connexion
  bool GPRS_MMS_Connected = false;
  while (!GPRS_MMS_Connected) {
    if ((gprsMMS.attachGPRS(GPRS_MMS_APN, GPRS_MMS_LOGIN, GPRS_MMS_PASSWORD) == GPRS_READY)) {
      GPRS_MMS_Connected = true;
    } else {
      info("GPRS \"mmsfree\" is connecting ...");
      delay(1000);
    }
  }

  // GPRS FREE connexion
  bool GPRS_FREE_Connected = false;
  while (!GPRS_FREE_Connected) {
    if ((gprsFREE.attachGPRS(GPRS_FREE_APN, GPRS_FREE_LOGIN, GPRS_FREE_PASSWORD) == GPRS_READY)) {
      GPRS_FREE_Connected = true;
    }
    else {
      info("GPRS \"free\" is connecting ...");
      delay(1000);
    }
  }

  // Force no low power mode
  gsmAccess.noLowPowerMode();

  // Send a sms when you're up
  smsToDebugNumbers("I'm up!");

#ifdef UCS2
  // Send an info if mode is UCS2
  info("UCS2 mode has been detected");
#endif

  // Debug on setup end
  debug("SMS setup OK");
}


/*
   Check if new sms are available

*/
void smsLoop() {
  // If there are any SMSs available()
  if (const int smsLength = sms.available()) {
    debug("sms.available() has been passed");
    // Get remote number
    sms.remoteNumber(senderNumber, 49);
#ifdef UCS2
    decodeUCS2(senderNumber);
    debug("decodeUCS2");
#endif

    // Send infos
    info("A message has been received from :");
    info(senderNumber);

    // Send debug
    debug("sms length is :");
    debug(smsLength);

    // An example of message disposal
    // Any messages starting with # should be discarded
    if (sms.peek() == '#') {
      info("A message starting with '#' was discarded");
      sms.flush();
      return;
    }

    // Array to store sms
    char data[smsLength + 1];

    // UCS2 check token
    bool isUCS2 = true;

    //Concat characters in the data array
    for (uint8_t i = 0; i < smsLength; i++) {
      data[i] = char(sms.read());
      if (isUCS2) isUCS2 = isDigit(data[i]);
    } data[smsLength] = '\0';

    // Check if MODEM force UCS2 mode
    if (data[0] != '0' || data[1] != '0' || (smsLength % 4) != 0) {
      isUCS2 = false;
    } else {
#ifndef UCS2
      info("It seems that this message was UCS2 encoded");
      info("Trying to decode it ...");
#endif
      decodeUCS2(data);
    }

    // Send the msg
    message(data);

    // Delete message from modem memory
    sms.flush();
  }
}


/*
   Send a message to all debug numbers

*/
void smsToDebugNumbers(char *data) {
  for (byte i = 0; i < sizeof(debugNumber) / 4; i++) {
    sms.beginSMS(debugNumber[i]);
    sms.print(data);
    sms.endSMS();
  }
}


/*
   Decode UCS2 data

*/
void decodeUCS2(char *data) {
  // Get sms length
  const uint8_t smsLength = strlen(data);

  // Array to store UCS2 code
  char ucsCode[smsLength];

  // Copy message data
  // to the UCS2 msg array
  strcpy(ucsCode, data);

  // Clean the data array
  for (uint8_t i = 0; i < smsLength; i++) {
    data[i] = '\0';
  }

  // Array to store UCS2 characters
  char ucsd[4 + 1];

  // Concat characters in the data array
  for (uint8_t i = 0; i < smsLength; i++) {
    ucsd[i % 4] = char(ucsCode[i]);
    if ((i % 4) == 3) {
      ucsd[4] = '\0';
      debug("UCS2 code is : ");
      debug(ucsd);
      for (uint8_t j = 0; j < 137; j++) {
        if (ucsd == ucsRaw[j]) {
          data[i / 4] = ucsOut[j];
          debug("USC2 code match :");
          debug(ucsOut[j]);
        }
      }
    }
  }
  data[smsLength] = '\0';
}
