#include "slip.h"


/*
 * Serial Line Internet Protocol (SLIP) setup
 * Used to send datas to Max thru USB
 * 
 */
void slipSetup() {
  // Start Serial COM
  Serial.begin(115200);

  // Wait for Serial
  while (!Serial);

  // Debug on setup end
  debug("SLIP setup OK");
}


/*
 * Begin a SLIP package
 *  With start frame
 *  
 */
void slipBeginPacket() {
  Serial.write(SLIP_EOT);
}


/*
 * Begin a SLIP package
 *  With end frame
 *  
 */
void slipEndPacket() {
  Serial.write(SLIP_EOT);
}


/*
 * Send a SLIP package thru USB (Serial)
 *  @addr : OSC address
 *  @data : Message (char* or int32_t)
 *  
 */
void slipSend(char *addr, char *data) {
  // Create a new msg
  OSCMessage msg(addr);

  // Add data
  // to its msg
  msg.add(data);

  // Send package
  slipBeginPacket(); 
    msg.send(Serial);
  slipEndPacket(),

  msg.empty(); // free space occupied by message
}

void slipSend(char *addr, int32_t data) {
  // Create a new msg
  OSCMessage msg(addr);

  // Add data
  // to its msg
  msg.add(data);

  // Send package
  slipBeginPacket(); 
    msg.send(Serial);
  slipEndPacket(),

  msg.empty(); // free space occupied by message
}


/*
 * Send an info thru SLIP
 *  @data : message (char* or int32_t)
 * 
 */
void info(char *data) {
#ifdef INFO
  slipSend("/info", data);
#endif
}

void info(int32_t data) {
#ifdef INFO
  slipSend("/info", data);
#endif
}


/*
 * Send a debug thru SLIP
 *  @data : message (char* or int32_t)
 * 
 */
void debug(char *data) {
#ifdef DEBUG
  slipSend("/debug", data);
#endif
}

void debug(int32_t data) {
#ifdef DEBUG
  slipSend("/debug", data);
#endif
}


/*
 * Send a sms thru SLIP
 *  @data : message
 * 
 */
void message(char *data) {
  slipSend("/sms", data);
}

void message(int32_t data) {
  slipSend("/sms", data);
}
