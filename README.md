# Pendule Etrange

GSM (sms) to Serial Line Internet Protocol (SLIP) Arduino firmware.

## Un-lock your PIN + GPRS login

Fill in the dedicated fields in the secrets.h file.

## Add your number to debug

Edit the sms.h file :

```c++
// ...

// Phone numbers to send debug infos
const char debugNumber[][20] = { "" };

// Example :
// { "+33612345678", "+336...", etc. }

// ...
```

## Enable UCS2 mode

This permit you to get accentuated letters to become non-null.

1. Edit your GSM_SMS.cpp and as below :

```c++
// ...

int GSM_SMS::available()
{
  /* ENABLE UCS2 MODE */
  MODEM.send("AT+CSCS=\"UCS2\"");

  if (_incomingBuffer.length() != 0) {

// ...
```

2. Un-comment the UCS2 definition in the ucs2.h file :

*You'll find it in your libraries/MKRGSM folder.*

```c++
// ...

#define UCS2 // Un-comment to force UCS2 mode

// ...
```

## Cycling'74 Max example

*You'll find the examples in the -Examples/ directory*

![](-Medias/Max-example.png)
