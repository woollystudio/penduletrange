#ifndef SMS_H__
#define SMS_H__

#include "Arduino.h"
#include <MKRGSM.h>
#include "slip.h"
#include "secrets.h"
#include "ucs2.h"

// Initialize the library instances
//extern GSMSSLClient client;
extern GPRS gprsMMS;
extern GPRS gprsFREE;
extern GSM gsmAccess;
extern GSM_SMS sms;

// PIN Number
const char PINNUMBER[] = SECRET_PINNUMBER;

// Phone numbers to send debug infos
const char debugNumber[][20] = { "+33625892192" };

// Example :
// { "+33612345678", "+336...", etc. }

// APN data
const char GPRS_MMS_APN[]      = SECRET_GPRS_MMS_APN;
const char GPRS_MMS_LOGIN[]    = SECRET_GPRS_MMS_LOGIN;
const char GPRS_MMS_PASSWORD[] = SECRET_GPRS_MMS_PASSWORD;
const char GPRS_FREE_APN[]      = SECRET_GPRS_FREE_APN;
const char GPRS_FREE_LOGIN[]    = SECRET_GPRS_FREE_LOGIN;
const char GPRS_FREE_PASSWORD[] = SECRET_GPRS_FREE_PASSWORD;

// Array to hold the number a SMS is retreived from
extern char senderNumber[];

// Foo's declaration
void smsSetup();
void smsLoop();
void smsToDebugNumbers(char *data);
void decodeUCS2(char *data);


#endif /* SMS_H__ */
