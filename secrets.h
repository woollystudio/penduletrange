#ifndef SECRETS_H__
#define SECRETS_H__

// See : https://www.testdedebit.fr/apn-free

#define SECRET_PINNUMBER          "1234"
#define SECRET_GPRS_MMS_APN       "mmsfree"  // replace your GPRS APN
#define SECRET_GPRS_MMS_LOGIN     "null"     // replace with your GPRS login
#define SECRET_GPRS_MMS_PASSWORD  "null"     // replace with your GPRS password
#define SECRET_GPRS_FREE_APN      "free"     // replace your GPRS APN
#define SECRET_GPRS_FREE_LOGIN    "null"     // replace with your GPRS login
#define SECRET_GPRS_FREE_PASSWORD "null"     // replace with your GPRS password


#endif /* SECRETS_H__ */
