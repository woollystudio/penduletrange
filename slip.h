#ifndef SLIP_H__
#define SLIP_H__

#include "Arduino.h"
#include <OSCMessage.h>

#define INFO  // Un-comment to send infos messages
#define DEBUG // Un-comment to send debug messages

#define SLIP_EOT 0xC0 // see: https://en.wikipedia.org/wiki/Serial_Line_Internet_Protocol

// Foo's declaration
void slipSetup();
void slipBeginPacket();
void slipEndPacket();
void slipSend(char *addr, char *data);
void slipSend(char *addr, int32_t data);
void info(char *info);
void info(int32_t info);
void debug(char *debug);
void debug(int32_t debug);
void message(char *sms);
void message(int32_t sms);


#endif /* SLIP_H__ */
