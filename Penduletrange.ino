/*
 *  Build with Arduino 1.8.16 (Teensyduino)
 *  for MKRGSM devices.
 *  Requires OSC (1.3.5) lib from CNMAT
 *  And MKRGSM (1.5.0) from Arduino.
 *  
 *  Project : Pendule Etrange, Christian Delecluse
 *  URL     : https://www.christiandelecluse.com/residence-a-la-maif-social-club/
 *  Author  : Martin Saez
 *  Date    : 14/01/2022
 *  
 */

#include "Penduletrange.h"

void setup() {
  // Wait for
  // Modem to start
  delay(1000);

  // Setup SLIP
  // communications
  slipSetup();

  // Setup SMS
  smsSetup();

  // Wait...
  delay(1000);
  info("Waiting for messages");

  // End of setup
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  // Main loop
  smsLoop();

  // Security
  //delay(1000); // against overflows ?

  // Ping to check if still here
  debug("ping");
}
